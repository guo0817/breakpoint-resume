// 获取输入框元素
const fileInput = document.getElementById('fileInput');
// 获取进度条
const progressBar = document.getElementById('progressBar');
// 监听器，选择文件后生效
fileInput.addEventListener('change', function () {
    selectedFile = fileInput.files[0];
});

/**
 * 分片上传文件的方法
 */
function startUpload() {
    if (!selectedFile) {
        alert('Please select a file to upload.');
        return;
    }

    //分片大小
    const chunkSize = 1024 * 1024; // 1MB chunks (adjust as needed)
    //分片起始大小
    let start = 0;
    let end = Math.min(chunkSize, selectedFile.size);
    //总分片数量
    const totalChunks = Math.ceil(selectedFile.size / chunkSize);
    let currentChunk = 1;

    //一个递归函数
    function uploadChunk() {
        const formData = new FormData();
        const chunk = selectedFile.slice(start, end);
        formData.append('file', chunk, selectedFile.name);
        formData.append('currentChunk', currentChunk);
        formData.append('totalChunks', totalChunks);
        const startTime = Date.now();
        var ajax = new XMLHttpRequest();
        ajax.open("post", formData);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        $.ajax({
            url: '/file/upload',
            type: 'POST',
            data: formData,
            processData: false, // 不处理数据
            contentType: false, // 不设置内容类型
            success: function (success) {
                if (success) {
                    currentChunk++;
                    progressBar.value = (currentChunk / totalChunks) * 100;

                    if (currentChunk <= totalChunks) {
                        const endTime = Date.now(); // 记录上传结束时间
                        const elapsedSeconds = (endTime - startTime) / 1000; // 以秒为单位的上传时间
                        const bytesUploaded = chunk.size; // 当前块的大小（字节数）
                        const speed = (bytesUploaded / 1024 / 1024 / elapsedSeconds).toFixed(2); // 上传速度（MB/s）
                        const percentage = ((currentChunk / totalChunks) * 100).toFixed(2); // 百分比

                        // 更新速度和百分比
                        document.getElementById('speed').textContent = `Speed: ${speed} MB/s`;
                        document.getElementById('percentage').textContent = `${percentage}%`;
                        uploadChunk();
                    } else {
                        alert('File upload completed!');
                    }
                } else {
                    alert('Chunk upload failed. Retrying...');
                    uploadChunk(); // Retry the same chunk
                }
            },
            error: function (error) {
                console.error('Error uploading chunk:', error);
            }
        });
    }

    uploadChunk();
}
