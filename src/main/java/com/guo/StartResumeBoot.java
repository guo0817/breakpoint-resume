package com.guo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
public class StartResumeBoot {

    public static void main(String[] args) {
        SpringApplication.run(StartResumeBoot.class, args);
    }

}
