package com.guo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

@RestController
@RequestMapping("/file")
public class FileUploadController {
    private static final String UPLOAD_DIR = "/Users/guoshuai/Documents/recent/pro";


    @PostMapping("/upload")
    public Boolean uploadFile(@RequestParam("file") MultipartFile file,
                                             @RequestParam("currentChunk") int currentChunk,
                                             @RequestParam("totalChunks") int totalChunks) {
        try {
            File uploadDir = new File(UPLOAD_DIR);

            if (!uploadDir.exists()) {
                uploadDir.mkdirs();
            }

            // 加上分块标识，以区分是分块
            String chunkedFileName = file.getOriginalFilename() + "_part" + currentChunk;

            File uploadedFile = new File(uploadDir, chunkedFileName);
            file.transferTo(uploadedFile);

            if (currentChunk == totalChunks) {
                // All chunks are uploaded, combine them into the final file
//                combineChunks(uploadDir, file.getOriginalFilename(), totalChunks);
                return true;
            } else {
                return true;
            }
        } catch (IOException e) {
            return false;
        }
    }

    private void combineChunks(File uploadDir, String originalFileName, int totalChunks) {
        try {
            File finalFile = new File(uploadDir, originalFileName);
            FileOutputStream outputStream = new FileOutputStream(finalFile);

            for (int i = 1; i <= totalChunks; i++) {
                String chunkedFileName = originalFileName + "_part" + i;
                File chunkFile = new File(uploadDir, chunkedFileName);
                FileInputStream inputStream = new FileInputStream(chunkFile);

                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                inputStream.close();
                chunkFile.delete(); // Delete the chunk file after combining
            }

            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/download/{filename}")
    public ResponseEntity<byte[]> downloadFile(@PathVariable String filename) {
        Path filePath = Paths.get(UPLOAD_DIR, filename);

        if (!Files.exists(filePath)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            byte[] fileContent = Files.readAllBytes(filePath);
            return ResponseEntity.ok()
                    .header("Content-Disposition", "attachment; filename=" + filename)
                    .body(fileContent);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
